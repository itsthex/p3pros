<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Mobile Inspections - P3: The Pre-Purchase Professionals</title> 
  <link rel="stylesheet" href="assets/css/select2.css" />
  <link rel="stylesheet" href="assets/css/style.css" />
</head>
<body>
  <div class="header-image"></div>
  <div class="header">
    <div class="wrapper">
      <span class="header-logo">//<em>P<sup>3</sup></em></span>
      <nav>
        <ul class="nav">
          <li><a href="#" class="link active">Home</a></li>
          <li><a href="#" class="link">Request Appointment</a></li>
          <li><a href="#" class="link">Services</a></li>
          <li><a href="#" class="link">About Us</a></li>
          <li><a href="#" class="link">Contact</a></li>
        </ul>
      </nav>
    </div>
  </div>
  <!-- intro section -->
  <div class="welcome-wrapper">
    <div class="welcome">
      <span>for a <em>good car</em><br />you need<br />a <em>great inspection</em></span>
    </div>
  </div>
  <div class="space">
    <div class="request">
      <div class="rtab">REQUEST AN APPOINTMENT</div>
      <form action="" class="reqform">
        <div class="common-float-33">
          <label for="name">Name</label>
          <input type="text" name="name" placeholder="NAME" data-validation-error="Please enter your name. Must be more than 1 letter.">
        </div>
        <div class="common-float-33">
          <label for="email">E-mail</label>
          <input type="text" name="email" placeholder="EMAIL" data-validation-error="Please enter a valid E-mail address.<br />Ex: john-doe@email.org">
        </div>
        <div class="common-float-33">
          <label for="zip-code">Zip Code</label>
          <input type="text" name="zip-code" placeholder="ZIP CODE" data-validation-error="Please enter a 5 digit Zip-Code">
        </div>
        <div class="vehicle-info">
          <div class="common-float-33 make">
            <select name="make" data-validation-error="Please select vehicle Make">
              <option value="-1">SELECT MAKE</option>
           </select>
          </div>
          <div class="common-float-33 model">
            <select name="model" data-validation-error="Please select vehicle Model">
              <option value="-1">MODEL</option>
            </select>
          </div>
          <div class="common-float-33 year">
            <select name="year" data-validation-error="Please select vehicle Year">
              <option value="-1">YEAR</option>
            </select>
          </div>
        </div>
        <div class="common-float-33 phone">
          <label for="phone">Phone Number</label>
          <input type="text" name="phone" placeholder="(000) 000-0000" data-validation-error="Please enter a 10 digit phone number<br/>Ex: 8189725546 or (818)7972-5546">
        </div>
        <div class="common-float-33 commentbox">
          <label for="comments">Comments (Vehicle specifics, desired date/time, additional information)</label>
          <div class="comments-description">
          Please enter any additional comments, questions, or requests here.<br />
          Example:<br />
          - Specifics about this vehicle<br />
          - Desired inspection date and time<br />
          - Additional information on location or vehicle availability
          </div>
          <textarea name="comments"></textarea>
        </div>
        <div class="common-float-33">
          <input type="button" name="submit" value="REQUEST INSPECTION">
        </div>
      </form>
    </div>
    <h2>WHAT IS <em class="logo">P<sup>3</sup></em>?</h2>
    <div class="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod odio nec tellus convallis semper. Proin at nisl ligula. Aenean tristique ligula sed mi pulvinar malesuada. Morbi pharetra odio magna, non tincidunt purus commodo vitae. Sed laoreet et lectus eget hendrerit. Pellentesque eu ipsum sem. Ut at turpis imperdiet, efficitur turpis sed, fringilla neque. Sed cursus ex arcu, vestibulum dictum dolor venenatis sit amet. Maecenas nec elit ut lorem tempor porttitor id et massa.</div>
    <div class="services">
      <div class="wrapper">
        <div class="service-list">
          <span class="service-text">Our<br/>Services</span>    
          <div class="common-float-33"><a href="#" class="abox service-inspection">Pre-Purchase Inspection</a></div>
          <div class="common-float-33"><a href="#" class="abox service-pre-sale">Pre-Sale Inspection</a></div>
          <div class="common-float-33"><a href="#" class="abox service-transport">Transport Inspection</a></div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
    <div class="serving">
      <div class="serving-image"></div>
      <div class="area">
        <h3>Serving Southern California</h3>
        <ul>Proudly serving the following counties:
          <li class="abox">Orange</li>
          <li class="abox">Los Angeles</li>
          <li class="abox">Riverside</li>
          <li class="abox">San Diego</li>
          <li class="abox">San Bernardino*</li>
        </ul>
        <span class="legend covered">= Appointments available</span>
        <span class="legend inquire">= Inquiries welcome</span>
        <span class="legend asterisk">Distance dependent</span>
      </div>
    </div>
    <div class="process">
      <div class="wrapper clear">
        <h3 class="process-title">The Process:</h3>
        <div class="process-icon-container"><span class="process-icon set-appointment">&#xf073;</span><span class="process-description">Request an Appointment</span></div>
        <div class="process-icon-container"><span class="process-icon arrow">&#xf054;</span></div>
        <div class="process-icon-container"><span class="process-icon confirm-details"><sup>&#xf086;</sup>&#xf271;</span><span class="process-description">Confirm the Details</span></div>
        <div class="process-icon-container"><span class="process-icon arrow">&#xf054;</span></div>
        <div class="process-icon-container"><span class="process-icon perform-inspection">&#xf1b9;<sup>&#xf002;</sup></span><span class="process-description">Inspection is Performed</span></div>
        <div class="process-icon-container"><span class="process-icon arrow">&#xf054;</span></div>
        <div class="process-icon-container"><span class="process-icon receive-results"><sub>&#xf01c;</sub><sup>&#xf0e0;</sup></span><span class="process-description">Receive the Results!</span></div>
      </div>
    </div>
    <div class="inspection-basic">
      <h2>SOME THINGS WE CHECK:</h2>
      <div class="points">
        <div class="inspection engine">
          <ul class="check">
            <li>Element One</li>
            <li>Element Two</li>
            <li>Element Three</li>
          </ul>
          <span class="right-point"></span>
        </div>
        <div class="inspection chassis">
          <ul class="check">
            <li>Element One</li>
            <li>Element Two</li>
            <li>Element Three</li>
          </ul>
          <span class="left-point"></span>
        </div>
        <div class="inspection functional">
          <ul class="check">
            <li>Element One</li>
            <li>Element Two</li>
            <li>Element Three</li>
          </ul>
          <span class="right-point"></span>
        </div>
        <div class="inspection body">
          <ul class="check">
            <li>Element One</li>
            <li>Element Two</li>
            <li>Element Three</li>
          </ul>
          <span class="left-point"></span>
        </div>
        <div class="inspection powertrain">
          <ul class="check">
            <li>Element One</li>
            <li>Element Two</li>
            <li>Element Three</li>
          </ul>
          <span class="right-point"></span>
        </div>
        <div class="inspection electrical">
          <ul class="check">
            <li>Element One</li>
            <li>Element Two</li>
            <li>Element Three</li>
          </ul>
          <span class="left-point"></span>
        </div>
      </div>
      <a class="abox medium sample-report">See Sample Report</a>
    </div>
  </div>
  <div class="foot">
    <div class="wrapper clear">
      <nav>
        <ul class="common-float-33 footer-links">
          <h4>Navigation</h4>
          <li><a href="#">Request Appointment</a></li>
          <li><a href="#">Services</a></li>
          <li><a href="#">About Us</a></li>
          <li><a href="#">Contact</a></li>
          <li><a href="#">Example Report</a></li>
        </ul>
      </nav>
      <div class="common-float-33 footer-center">
        <h3>Pre-Purchase Professionals</h3>
        <h2><em class="logo footer">P<sup>3</sup></em></h2>
        <h3 class="footer-email"><a href="#">inquiries@prepurchasepros.com</a></h3>
      </div>
      <div class="common-float-33 footer-notes">
        <h4>Inspection Policy</h4>
        <span>Pre-Purchase Professionals reserves the right to refuse an inspection on any vehicle for any reason. Not all vehicles are the same, so inspection times may vary.</span>
      </div>
    </div>
  <div class="copyright clear">&copy; 2017 Pre-Purchase Professionals - PrePurchasePros.com</div>
  </div>
  <div class="form-error"></div>
  <script src="https://use.fontawesome.com/37465b4c58.js"></script>
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/select2min.js"></script>
  <script src="assets/js/app.js"></script>
</body>
</html>