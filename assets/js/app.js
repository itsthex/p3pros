// Form related script
(function($){
	$(document).ready(function() {
		var $fieldBox = $('.reqform .common-float-33');
		var vehicleData;
		var $selectMakes = $('[name=\'make\']');
		var $selectModels = $('[name=\'model\']');
		var $selectYears = $('[name=\'year\']');
		var vehicleMakes = [];
		var $error = $('.form-error');
		// Phone number input parsing
		function parsePhone($name) {
		  var len = $name.val().length;
		  var val = $name.val();
		  var no = [40,41,45,32];
		  var x, st;
		  var i = 0;
		  for (x = 0; x < len; x++) {
		    if (no.indexOf(val.charCodeAt(x)) === -1 ) {
		      i++;
		      st = (st) ? st+val.charAt(x) : val.charAt(x);
		    }
		  }
		  return [i,st];
		}
		function keyNum($name,key,len,phone) {
		  var char = key.keyCode;
		  var str = parsePhone($name);
		  var yes = [48,49,50,51,52,53,54,55,56,57];
		  if (phone) { yes.push(40,41,45,32); }
		  if (yes.indexOf(char) !== -1 && str[0] < len) {
		    if (str[0] < 1) { return (char !== 48 && char !== 49) ? true : false; }
		    return true;
		  }
		}
		function popError($name) {
			$error.css({ top: ($name.offset().top+46)+'px', left: $name.offset().left+'px' });
			$error.html($name.data("validationError"));
			$error.show();
		}
		function removeError() {
		  $error.hide();
		}
		function phoneBlur($box,$name) {
		  var str = parsePhone($name);
		  var st = (str[1].length) ? '(' : null;
		  var x;
		  for (x = 0; x < str[0]; x++) {
		    st = st+str[1].charAt(x);
		    if (x === 2) { st = st+') '; }
		    if (x === 5) { st = st+'-'; }
		  }
		  if (str[1].length < 10) { $name.val(''); $box.removeClass('field-active'); popError($name,'phone'); } else { $name.val(st); }
		}
		function doCheck() {
			$fieldBox.find('input, select, textarea').each(function() {
				var $this = $(this);
				switch (this.name) {
					case 'name':
						if ($this.val().length < 2) {
							popError($this);
							$this.focus();
							return false;
						}
						break;
					case 'email':
						var reg = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
						if (!$this.val().length || !reg.test($this.val())) {
							popError($this);
							$this.focus();
							return false;
						}
						break;
					case 'zip-code':
						if ($this.val().length < 5) {
							popError($this);
							$this.val('');
							$this.focus();
							return false;
						}
						break;
					case 'phone':
						var num = parsePhone($this);
						if(num[0] < 10) {
							popError($this);
							$this.focus();
							return false;
						}
						break;
					case 'make':
					case 'model':
					case 'year':
						if($this.val() < 0) {
							popError($this);
							$this.focus();
							return false;
						}
					default:
						return true;
				}
				return true;
			});
		}
		// Vehicle data parser
		function parseVehicleData(make,model) {
			var x;
			if (make&&!model) {
				var vehicleModels= [];
				for (x =0; x < vehicleData.makes[make].models.length; x++) {
					vehicleModels.push({ id: x, text: vehicleData.makes[make].models[x].name });
				}
				return vehicleModels;
			} else if (make&&model) {
				var vehicleYears= [];
				for (x =0; x < vehicleData.makes[make].models[model].years.length; x++) {
					vehicleYears.push({ id: x, text: vehicleData.makes[make].models[model].years[x].year });
				}
				return vehicleYears;
			} else {
				var length = vehicleData.makes.length;
				if (length > 0) {
					for (x = 0; x < length; x++) {
						vehicleMakes.push({ id: x, text: vehicleData.makes[x].name });
					}
				}
			}
		}
		// Called after AJAX is complete, starts data sort
		function listInit() {
			parseVehicleData();
			$selectMakes.select2({
				data: vehicleMakes
			});
			$selectModels.select2({ disabled: true });
			$selectYears.select2({ disabled: true });
		}
		// First task, grab vehicle data
		$.ajax({
			url: './assets/vehicle-data/edmunds-basic-makes.json',
			type: 'GET',
			dataType: 'JSON',
			error: function () {
				console.log('Uh-oh');
			},
			success: function(data) {
				if(data.makes.length > 0) {
					vehicleData = data;
					listInit()
				}
			}
		});
		// Make/Model/Year form Select2 actions
		$selectMakes.on("select2:select", function(){
			if($(this).val() >= 0) {
				removeError();
				$selectModels.empty().select2();
				var data = parseVehicleData($(this).val());
				data.unshift({ id: -1, text: 'SELECT MODEL' });
				$selectModels.select2({
					data: data,
					disabled: false
				})
				$selectModels.focus();
				$selectModels.select2('open');
				$selectYears.empty().select2();
				$selectYears.select2({
					placeholder: 'YEAR',
					disabled: true
				});
			} else {
				$selectModels.empty().select2();
				$selectModels.select2({
					placeholder: 'MODEL',
					disabled: true
				});
				$selectYears.empty().select2();
				$selectYears.select2({
					placeholder: 'YEAR',
					disabled: true
				});
			}
		});
		$selectModels.on("select2:select", function(){
			if($(this).val() >= 0) {
				removeError();
				$selectYears.empty().select2();
				var data = parseVehicleData($selectMakes.val(),$(this).val());
				$selectYears.select2({
					data: data,
					disabled: false
				});
				$selectYears.focus();
				$selectYears.select2('open');
			} else {
				$selectYears.empty().select2();
				$selectYears.select2({
					placeholder: 'YEAR',
					disabled: true
				});
			}
		});
		// Field Checks and actions
		$fieldBox.each(function() {
			var $this = $(this);
			var $input = $this.find('input, select, textarea');
			var $commentBox = $this.find('.comments-description');
			var numerics = ['phone','zip-code'];
			$input.on({
				keyup: function(e) {
					var numeric = numerics.indexOf(this.name) !== -1;
					if([8,46].indexOf(e.keyCode) !== -1 && $(this).val() == '' && this.nodeName !== 'TEXTAREA') {
						$this.removeClass('field-active');
					} else if(e.keyCode > 46 && !numeric) {
						$this.addClass('field-active');
					} else if(numeric && $(this).val() !== '') {
						$this.addClass('field-active');
					}
				},
				keypress: function(e) {
					removeError();
					var input_name = this.name;
					var numeric = numerics.indexOf(input_name) !== -1;
					if(numeric) {
						return (input_name == 'phone' && keyNum($(this),e,10,true)) ? true : (input_name == 'zip-code' && keyNum($(this),e,5)) ? true : false;
					}
				},
				focus: function() {
					if (this.nodeName == 'TEXTAREA') {
						$commentBox.hide();
						$this.addClass('field-active');
					}
				},
				click: function() {
					removeError();
					if (this.name === 'submit') { doCheck(); }
				},
				blur: function() {
					var $name = $(this);
					if (this.nodeName == 'TEXTAREA' && $name.val() == '') {
						$commentBox.show();
						$this.removeClass('field-active');
					} else if(this.name === numerics[0] && $name.val()) {
						phoneBlur($this,$name);
					} else if(this.name === numerics[1] && $name.val().length < 5) {
						popError($name,'zip');
						$name.val('');
						$this.removeClass('field-active');
					}
				}
			});
			$commentBox.click(function() {
				$(this).hide();
				$('textarea[name=\'comments\']').focus();
				$('.commentbox').addClass('field-active');
			});
		});
	});
})(window.jQuery);